const { authSecret } = require('../../.env')
const jwt = require('jwt-simple')
const bcrypt = require('bcrypt-nodejs')


module.exports = app => {

  const signin = async (req, res) => {
    
    if ( req.body.lang )
      app.i18n.setLocale(req.body.lang)
    // verifica a req
    if ( !req.body.email )
      return res.status(400).json( {'error': app.i18n.__('empty_email')} )

    if ( !req.body.password )
      return res.status(400).json( {'error': app.i18n.__('empty_password')} )  

    const user = await app.db('users')
    .where( {email: req.body.email.toLowerCase()}  )
    .first()

    if ( user ) {

      bcrypt.compare(req.body.password, user.password , (error, isMatch) => {
      
        if ( error || !isMatch )
          return res.status(401).json( { 'error': app.i18n.__('wrong_password') } )


        const payload = { id: user.id }  

        const userData = {
          name: user.name,
          email: user.email,
          token: jwt.encode(payload, authSecret)
        }

        return res.status(200).json( userData )


      } )

    } else {
      return res.status(404).json( {'error': app.i18n.__('user_not_found')} )
    }

  }

  return { signin }
}
