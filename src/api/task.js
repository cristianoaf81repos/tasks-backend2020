const moment = require('moment')

module.exports = app => {

  const getAllTasks = ( req, res ) => {

    if ( req.query.lang )
      app.i18n.setLocale(req.query.lang)
    
    app.db('tasks')  
    .where( { userId: req.user.id } )
    .orderBy('estimateAt')
    .then( tasks => {

      if ( tasks.length > 0 )
        res.status(200).json(tasks)
      else
        res.status(400).json( {'message': app.i18n.__('no_tasks_found')} )

    })
    .catch(error => {

      if ( error )
        res.status(500).json( {'message': app.i18n.__('user_and_data_not_found')})
        
    })
  }

  const getTasks = (req, res) => {
   
    if ( req.query.lang )
      app.i18n.setLocale(req.query.lang)

    //moment().endOf('day').toDate()
    let date = req.query.date ? req.query.date : moment().endOf('day').toDate()
   
    if ( !date )
      date = new Date()

    app.db('tasks')
    .where({ userId: req.user.id })
    .where('estimateAt', '<=', date )
    .orderBy('estimateAt')
    .then( tasks => {

      if ( tasks.length > 0 )
        res.status(200).json(tasks)
      else
        res.status(400).json( {'message': app.i18n.__('no_tasks_found')} )

    } ).catch( error => {
       if ( error )
        res.status(500).json( {'message': app.i18n.__('user_and_data_not_found')})       
    })

  }

  const save = (req, res) => {

    if ( req.body.lang ) 
      app.i18n.setLocale(req.body.lang)
      
        
    if ( !req.body.desc.trim() )
      return res.status(400).json( { 'error': app.i18n.__('no_task_description') } )

    
    // seta o id do usuario a partir do obj user do passport  
    req.body.userId = req.user.id
    

    app.db ('tasks')
    .where( { desc: req.body.desc.trim(), userId: req.user.id } ) // vrfk c task com desc. ja existe
    .first()
    .then( task => { 

        if ( task )

          return res.status(400).json( { 'error' : app.i18n.__('task_already_exists',`${req.body.desc.trim()}`)})
          
        else {
          const taskData = {
            userId: req.body.userId,
            estimateAt: req.body.estimateAt ? req.body.estimateAt : null,
            desc: req.body.desc.trim(),
            doneAt: req.body.doneAt ? req.body.doneAt : null
          }     
          
          app.db('tasks')
          .insert(taskData)
          .then( _ => res.status(200).json({'message': app.i18n.__('task_success_saved')}) )
          .catch( _ => res.status(500).json( {'error': app.i18n.__('failed_to_save_task_data')} ))
        }
     } )
    

  }

  const remove = ( req, res ) => {
    
    if ( req.body.lang ) 
      app.i18n.setLocale(req.body.lang)

    if ( !req.params.id )
      return res.status(400).json( { 'error': app.i18n.__('no_task_id_sent') } )

    app.db ('tasks')
    .where ( { id: req.params.id , userId: req.user.id } )
    .del()
    .then( rowsDeleted => {
      
      if ( rowsDeleted > 0 )
        res.status(200).json( { 'message': app.i18n.__('task_remove_success') } )
      else 
        res.status(400).json( { 'error': app.i18n.__('cannot_find_task',`${req.params.id}`) } )

    } )
    .catch( _ => res.status(500).json( { 'error': app.i18n.__('fail_remove_task') } ) )

  }

  const updateTaskDoneAt = ( req, res, doneAt ) => {
    app.db('tasks')
    .where( { id: req.params.id, userId: req.user.id } )
    .update( { doneAt } )
    .then( _ =>  res.status(200).json( {'message': app.i18n.__('task_doneat_updated_successfully') } ) )
    .catch( _ => res.status(500).json( {'error': app.i18n.__('task_doneat_update_fail') } ) )
  }

  const toggleTask = ( req, res ) => {
    
    if ( req.body.lang ) 
      app.i18n.setLocale(req.body.lang)

    if ( !req.params.id )
      return res.status(400).json( {'error': app.i18n.__('no_task_id_sent_param')} )
    
    app.db('tasks')  
    .where( {id: req.params.id, userId: req.user.id } )
    .first()
    .then( task => {
      if ( !task )
        return res.status(400).json( {'error': app.i18n.__('no_task_found',`${req.params.id}`)} )   
      
      const doneAt = task.doneAt ? null : new Date()  
      updateTaskDoneAt(req, res, doneAt)
        
    })
    .catch( _ => {
      res.status(500).json( {'error': app.i18n.__('fail_to_retrive_task_data') } )
    })

  }

  return { getAllTasks, getTasks, save, remove, toggleTask }

}