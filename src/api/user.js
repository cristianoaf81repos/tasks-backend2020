const bcrypt = require('bcrypt-nodejs')


module.exports = app => {
  
  // aplica hash na senha
  const getHash = (password, callback) => {

    bcrypt.genSalt(10, (error, salt) => {

      bcrypt.hash(password, salt, null, (error,hash) => callback( hash ))

    })

  }

  // salva usuario no bd
  const save = ( req, res ) => {  

    if ( req.body.lang )
      app.i18n.setLocale(req.body.lang)

    // checa a req
    if ( !req.body.name )
      return res.status(400).json({'error': app.i18n.__('user_name_not_provided') })

    if ( !req.body.email )
      return res.status(400).json({'error': app.i18n.__('email_not_provided')})

    if ( req.body.email && !req.body.email.match(/^[a-z0-9]+@[a-z0-9]+\.[a-z]/) )  
    return res.status(400).json({'error':app.i18n.__('invalid_email')})

    if ( !req.body.password )
      return res.status(400).json({'error':app.i18n.__('password_not_provided')})

    // password com hash
    getHash(req.body.password, hash => {

      const userData = {
        name: req.body.name,
        email: req.body.email,
        password: hash
      }

                 
      app.db('users')
      .insert(  userData  )
      .then( _ => res.status(201).json({'message':app.i18n.__('user_success_saved')}) )
      .catch( error => {

        error.sqlMessage ?        
        res.status(400).json({'error':  app.i18n.__('user_already_exists') })  :
        res.status(500).json({'error': app.i18n.__('user_generic_error_message') })

      } )
     

    })



  }

  return { save }

}