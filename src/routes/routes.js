var paths = require('../api/paths')

module.exports = app => {
  
  app.post( paths.SIGNUP , app.src.api.user.save )
  app.post ( paths.SIGNIN, app.src.api.auth.signin )
  
  app.route( paths.TASKS )
  .all( app.src.config.passport.authenticate() )
  .get( app.src.api.task.getTasks )
  .post( app.src.api.task.save )

  app.route( paths.TASKS_WITH_ID )
  .all( app.src.config.passport.authenticate() )
  .delete( app.src.api.task.remove )

  app.route( paths.TOGGLE_TASK )
  .all( app.src.config.passport.authenticate() )
  .put( app.src.api.task.toggleTask )

  app.route( paths.ALL_TASKS )
  .all( app.src.config.passport.authenticate() )
  .get( app.src.api.task.getAllTasks )
  
}