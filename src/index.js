const app = require('express')()
const portconfig = require('./config/portconfig')
const port = portconfig.normalizePort(process.env.PORT || '3000')
const db = require('./config/db')
const consign = require('consign')
const i18n = require('i18n')

consign()
.include('./src/config/passport.js') // inclui o middleware de seguranca na aplicacao
.then('./src/config/middlewares.js') // carrega os middlewares genericos
.then('./src/api') // carrega os middlewares especificos da api
.then('./src/routes/routes.js') // carrega as rotas da aplicacao
.into( app )

app.db = db
app.i18n = i18n


app.listen(port, ()=> {
  console.log('backend rodando!')
})
