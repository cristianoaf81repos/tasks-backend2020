const { authSecret } = require('../../.env')
const passport = require('passport')
const passportJwt = require('passport-jwt')
const { Strategy, ExtractJwt } = passportJwt


module.exports = app => {

  const params = {
    secretOrKey: authSecret,
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  }

  const sqlError = 'user not found!'
  const genericErrorMessage = 'fail to access user data, please try again later!'

  const strategy = new Strategy( params, ( payload, done ) => {

    app.db ('users')
    .where ( { id: payload.id } )
    .first ()
    .then ( user =>  {

      if ( user )
      
        done(null, { id: user.id, email: user.email })

      else {

        done(null, false)
        

      }
      
    } ) .catch ( error  => 
      error.sqlMessage ? 
      res.status(400).json({'error': sqlError}) :
      res.status(500).json( {'error': genericErrorMessage } )
    )

    

  })

  passport.use( strategy )

  

  return {
    initialize: () => passport.initialize(),
    authenticate: () => passport.authenticate('jwt', { session: false })
  
  }

}