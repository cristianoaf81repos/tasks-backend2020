const bodyParser = require('body-parser')
const cors = require('cors')
const compression = require('compression')
const cookieParser = require('cookie-parser')
const i18n = require('i18n')


i18n.configure({
  locales: ['en','pt-br'],
  defaultLocale: 'en',
  directory: __dirname + '/locales'
})

//i18n.setLocale('pt-br')


module.exports = app => {
  app.use(  bodyParser.json() ) // body req em json
  app.use( cors( {origin: '*'} ) ) // cors em qualquer origem
  app.use( cookieParser() ) // habilita cookies 
  app.use ( i18n.init ) // multiplos idiomas na api
  app.use( compression() ) // compressao gzip
}