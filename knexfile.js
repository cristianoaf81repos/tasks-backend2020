// Update with your config settings.
// process.env.MYSQL_HOST host do bd mysql ou usar localhost
module.exports = {
 
  client: 'mysql2',
  connection: {
    host: process.env.MYSQL_HOST || 'localhost',
    database: 'tasks',
    user:     'root',
    password: 'mysqladmin'
  },
  pool: {
    min: 2,
    max: 10
  },
  migrations: {
    tableName: 'knex_migrations'
  }  

}
