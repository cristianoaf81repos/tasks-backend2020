
exports.up = async function(knex) {
  return await knex.schema.createTable('tasks', table => {
    table.increments('id').primary()
    table.string('desc').notNull()
    table.datetime('estimateAt')
    table.datetime('doneAt')
    table.integer('userId').unsigned()// exclusivo para mysql

    table.foreign('userId').references('users.id')// exclusivo para mysql
  })  
}

exports.down = async function(knex) {
  return await knex.schema.dropTable('tasks')
}
