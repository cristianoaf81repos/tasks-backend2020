
exports.up = async function(knex) {

  return await knex.schema.createTable('users', table => {
    table.increments('id').primary()
    table.string('name').notNull()
    table.string('email').notNull().unique()
    table.string('password').notNull()
  })

}

exports.down = async function(knex) {
  return await knex.schema.dropTable('users') 
}
