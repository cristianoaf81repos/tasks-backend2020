# Sobre  
   
Projeto backend node js e express. 
destinado a sistema de tarefas  
tipo To-Do app atualização (2020).  
  
# Ferramentas utilizadas.    
 
  - Node js.      
  - Express js.      
  - Consign.     
  - I18n.       
  - Body-parser.   
  - Knex.   
  - Mysql2.   
  - Passport.   
  - Passport jwt.  
  - Jwt-Simple.  
  - Nodemon.  
  - Pm2 (production mode).     
  - Visual stutio Code. 
  - Java Script.  

# Features.  
  
  Api com atenticação via token com endpoints  
  seguros, cors, banco de dados relacional   
  mysql já preparado para criação de container  
  Docker.
  
### Pontos de acesso.  
  
  - http post localhost:3000/signup  cadastro de usuário c/ validação de email.  
  - http post localhost:3000/signin  processo de autenticação ou login.  
  - http get  localhost:3000/tasks?date="datehere" obter tarefa baseando-se na data de início.  
  - http delete localhost:3000/tasks/:id remove uma tafera do usuário pelo identificador.  
  - http get localhost:3000/tasks/all obtem lista completa de todas as tarefas do usuário.  
  - http put localhost:3000/tasks/:id/toggle  define estado de conclusão da tarefa alterando o mesmo.  


# Estrutura do projeto.  
  
  '''  
    .  
    ├── migrations  
    ├── node_modules  
    ├── src  
    │   ├── api  
    │   ├── auth.js  
    │   ├── paths.js  
    │   ├── task.js  
    │   └── user.js  
    ├── config  
    │   ├── db.js  
    │   ├── locales  
    │   │   ├── br.json  
    │   │   ├── en.json  
    │   │   └── pt-br.json  
    │   ├── middlewares.js  
    │   ├── passport.js  
    │   └── portconfig.js  
    ├── index.js  
    └── routes  
        └── routes.js  
  '''  
    
# Instruções.  
  
  - Clonar este repositório.    
    - git clone https://profcristianoaf81@bitbucket.org/cristianoaf81repos/tasks-backend2020.git  
  - Acessar diretório.  
    - cd tasks-backend2020   
    - npm install.  
    - npm start
  Obs: certifique-se que a porta 3000 não esteja em uso por outro processo do S.O.  
  ou altere o arquivo index.js.  
  
# Suporte.
   
  Este projeto possui finalidades educativas para tal não possui suporte,  
  pode ser baixado, compartilhado ou editado, mas não possui nenhuma garantia.  
  
# Licença.
  
  ## GNU GPLv3  
   [veja detalhes da licença](https://choosealicense.com/licenses/gpl-3.0/)    
     


  

  

